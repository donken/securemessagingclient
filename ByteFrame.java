package transport;

import java.io.Serializable;
public class ByteFrame implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -493080639907651015L;
	
	public byte[] data;
}