This program is built on Java 1.7 and is graphical-user-interface-based. While the command line works and the arguments specified work, the Graphical Interface is how you talk to the program and exchange information.

At the start, the program will ask you if you want to be BOB or ALICE, there are not two versions of this program, there is only one with this option. In addition, the program asks you for an IP address and port if you are BOB, or a port to listen on if you are ALICE. Fill these in accordingly.

