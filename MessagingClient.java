package transport;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class MessagingClient {
    
    private String headerItem = "Welcome to DXNET";
    private String headerWindow = "DXNET Secure Messaging Client";

	BufferedReader in;
    JFrame frame = new JFrame(headerWindow);
    JTextField textField = new JTextField(60);
    JTextArea messageArea = new JTextArea(24, 60);
    
    Socket PrimarySocket;
    
    boolean connected = false;
    static boolean useDebug = false;
    
    boolean applicationActive = true;
    
    boolean isAlice = false;

    /**
     * Constructs the client by laying out the GUI and registering a
     * listener with the textfield so that pressing Return in the
     * listener sends the textfield contents to the server.  Note
     * however that the textfield is initially NOT editable, and
     * only becomes editable AFTER the client receives the NAMEACCEPTED
     * message from the server.
     */
    public MessagingClient() {

        // Layout GUI
        textField.setEditable(false);
        messageArea.setEditable(false);
        frame.getContentPane().add(textField, "North");
        frame.getContentPane().add(new JScrollPane(messageArea), "Center");
        frame.pack();
        
        connected = false;

        // Add Listeners
        textField.addActionListener(new ActionListener() {
            /**
             * Responds to pressing the enter key in the textfield by sending
             * the contents of the text field to the server.    Then clear
             * the text area in preparation for the next message.
             */
            public void actionPerformed(ActionEvent e) {
            	
            	if (!isAlice)
            		return;
            	
            	ObjectOutputStream toOtherSide;
            	ObjectInputStream fromOtherSide;
            	try {
					toOtherSide = new ObjectOutputStream(PrimarySocket.getOutputStream());
				} catch (IOException e1) {
	                messageArea.append("System: I/O Error\n");
	                return;
				}
            	
            	ByteFrame scratchFrame = new ByteFrame();
            	try {
					scratchFrame.data = "SENDREQUEST".getBytes("UTF-8");
				} catch (UnsupportedEncodingException e3) {
					System.out.println("Bad encoding.");
					return;
				}
            	
            	//--------------------BEGIN SENDING-----------------------
            	
            	if (useDebug)
	        	{
	        		System.out.println("Alice is sending a message request.");
	        	}
            	
            	try {
            		// Notify "Bob" that we want to write a message
					toOtherSide.writeObject(scratchFrame);
					toOtherSide.flush();
				} catch (IOException e1) {
	                messageArea.append("System: I/O Error\n");
	                return;
				}
            	
            	byte[] scratch = null;
            	
            	//--------------------READ IN PUBLIC KEY-----------------------
            	
            	if (useDebug)
	        	{
	        		System.out.println("Alice is reading Bob's public key...");
	        	}
            	
            	try {
            		// Read both incoming messages to determine Bob's public key
            		fromOtherSide = new ObjectInputStream(PrimarySocket.getInputStream());
					scratchFrame = (ByteFrame) fromOtherSide.readObject();
					scratch = scratchFrame.data.clone();
					scratchFrame = (ByteFrame) fromOtherSide.readObject();
				} catch (ClassNotFoundException | IOException e4) {
					System.exit(-1);
				}
            	
            	//--------------------VERIFY PUBLIC KEY FROM BOB-----------------------
            	
            	if (useDebug)
	        	{
	        		System.out.println("Alice is verifying Bob's public key...");
	        	}
            	
            	try {
            		// Verify his signature
					SecureMessaging.VerifySigning(SecureMessaging.ReadPublicKey("AliceKeys/c_public_key.der"), scratch, scratchFrame.data);
				} catch (InvalidKeyException | NoSuchAlgorithmException
						| SignatureException | InvalidKeySpecException e4) {

					if (useDebug)
		        	{
		        		System.out.println("System: Signature exception\n");
		        	}
	                return;
				} catch (IOException e1) {
					if (useDebug)
		        	{
		        		System.out.println("System: I/O Error\n");
		        	}
					return;
				}
            	
            	//--------------------RESOLVE KEY FOR USE-----------------------
            	
            	if (useDebug)
	        	{
	        		System.out.println("Alice is resolving Bob's public key...");
	        	}
            	
            	PublicKey bobKey;
            	try {
					bobKey = SecureMessaging.ResolveKey(scratch);
				} catch (InvalidKeySpecException | NoSuchAlgorithmException e1) {
	                messageArea.append("System: Your platform does not support one or more protocols this program uses.\n");
	                return;
				}

            	//--------------------BUILD MESSAGE AND SEND TO BOB-----------------------
            	
            	if (useDebug)
	        	{
	        		System.out.println("Alice is building her message...");
	        	}
            	
            	String messageBuilder;
				messageBuilder = textField.getText();
				
            	String symBuilder;
            	ByteFrame messageFrame = new ByteFrame();
            	
            	try {
					messageBuilder = Base64.getEncoder().encodeToString(SecureMessaging.HashMessage(messageBuilder.getBytes()));
					messageBuilder = Base64.getEncoder().encodeToString(SecureMessaging.EncryptArrayWithKey(messageBuilder.getBytes(), 
							SecureMessaging.ReadPrivateKey("AliceKeys/private_key.der")));
					messageBuilder = messageBuilder + "&&x&&" + textField.getText();
					
					SecretKey secret = SecureMessaging.GenerateSymmetricKey();
					
					messageBuilder = Base64.getEncoder().encodeToString(SecureMessaging.EncryptArrayWithSymKey(messageBuilder.getBytes(), secret));
					
					symBuilder = Base64.getEncoder().encodeToString(SecureMessaging.EncryptArrayWithKey(secret.getEncoded(), bobKey));
					
					//messageArea.append(messageBuilder);
					ByteFrame symFrame = new ByteFrame();
					
					symFrame.data = symBuilder.getBytes();
					
					if (useDebug)
		        	{
		        		System.out.println("Alice is sending her frames...");
		        	}
					
					toOtherSide.writeObject(symFrame);
					toOtherSide.flush();

					messageFrame.data = messageBuilder.getBytes();

					toOtherSide.writeObject(messageFrame);
					toOtherSide.flush();
					
				} catch (NoSuchAlgorithmException | InvalidKeyException | 
						NoSuchPaddingException | IllegalBlockSizeException | 
						BadPaddingException | InvalidKeySpecException | IOException e1) {
					if (useDebug)
		        	{
		        		System.out.println("Your platform does not support one or more protocols this program uses.\n");
		        	}
					return;
				}
            	
                messageArea.append("Alice: " + textField.getText() + "\n");
                textField.setText("");
            }
        });
    }
    
    private int getUserType() {
    	//Custom button text
    	Object[] options = {"Alice (Sender)",
    	                    "Bob (Receiver)"};
    	return JOptionPane.showOptionDialog(frame,
    	    "Choose the user type you will be.",
    	    "User Selection",
    	    JOptionPane.YES_NO_OPTION,
    	    JOptionPane.QUESTION_MESSAGE,
    	    null,
    	    options,
    	    options[0]);
    }

    /**
     * Prompt for and return the address of the server.
     */
    private String getUserAddress() {
        return JOptionPane.showInputDialog(
            frame,
            "Enter IP Address of the User:",
            headerItem,
            JOptionPane.QUESTION_MESSAGE);
    }
    
    private int getUserPort()
    {
    	try {
    	return Integer.parseInt(JOptionPane.showInputDialog(
                frame,
                "Enter Port of the User:",
                headerItem,
                JOptionPane.QUESTION_MESSAGE));
    	} catch (NumberFormatException e)
    	{
    		messageArea.append("System: Bad port detected, defaulting to 9000");
    		return 9000;
    	}
    }
    
    private int getListeningPort()
    {
    	try {
        	return Integer.parseInt(JOptionPane.showInputDialog(
                    frame,
                    "Enter Port to listen on:",
                    headerItem,
                    JOptionPane.QUESTION_MESSAGE));
        	} catch (NumberFormatException e)
        	{
        		messageArea.append("System: Bad port detected, defaulting to 9000");
        		return 9000;
        	}
    }
    
    /**
     * Connects to the server then enters the processing loop.
     */
    private void run() throws IOException {

    	int userChoice = getUserType();
        Socket socket = null;
        
        // Split between "Alice" and "Bob" branches of program
        if (userChoice == 0)
        {
            int port = getListeningPort();
            ServerSocket listener;
        	messageArea.append("System: Waiting on connection...\n");
			listener = new ServerSocket(port);
	    	socket = listener.accept();
	    	listener.close();
	    	
	    	isAlice = true;
        } else {

            // Make connection and initialize streams
            String userAddress = getUserAddress();
            
            if (userAddress == null)
            {
            	System.exit(0);
            }
            
            // Open socket
            System.out.println(userAddress);
            int port = getUserPort();
            try {
            	socket = new Socket(userAddress, port);
            } catch (SocketException e)
            {
            	messageArea.append("System: IP address and port could not be resolved.");
            	return;
            }
        }
        
        if (isAlice)
        {
        	messageArea.append("System: You are Alice, you can send messages.\n");
        }
        else
        {
        	messageArea.append("System: You are Bob, you will receive messages.\n");
        }

    	messageArea.append("System: Connected to another user.\n");
        
        PrimarySocket = socket;
        
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        ObjectInputStream fromOtherSide = null;
        ObjectOutputStream toOtherSide;
        
    	connected = true;
    	textField.setEditable(isAlice);
    	
        while (applicationActive)
        {
        	// Alice stays in loop to keep program properly alive
        	if (isAlice)
        	{
        		continue;
        	}
        	
        	//--------------------BEGIN BOB'S LOOP-----------------------
        	
        	try
        	{
            toOtherSide = new ObjectOutputStream(socket.getOutputStream());
        	} catch (SocketException e)
        	{
        		messageArea.append("System: Connection has been terminated.\n");
        		return;
        	}
        	
        	//--------------------BEGIN RECEIVING "SENDREQUEST"-----------------------
            
        	ByteFrame frame = null, frame2 = null;
        	ByteFrame message = null;
			try {
	            fromOtherSide = new ObjectInputStream(socket.getInputStream());
				frame = (ByteFrame) fromOtherSide.readObject();
				if ((new String(frame.data).equals("SENDREQUEST")))
				{
					if (useDebug)
					{
						System.out.println("Bob received send request.");
					}
				}
				else
				{
					continue;
				}
				
				frame2 = new ByteFrame();
				
				//--------------------BEGIN SENDING BOB'S ENCODED KEY-----------------------
				
				if (useDebug)
	        	{
	        		System.out.println("Sending Bob's public key");
	        	}
				
				byte[] publicKey = SecureMessaging.ReadPublicKey("BobKeys/public_key.der").getEncoded();
				
				frame.data = publicKey;
				toOtherSide.writeObject(frame);
				toOtherSide.flush(); // Java requires flushing the stream between writes
				frame2.data = SecureMessaging.SignArrayWithPrivateKey(publicKey, 
						SecureMessaging.ReadPrivateKey("BobKeys/c_private_key.der"));
				toOtherSide.writeObject(frame2);
				toOtherSide.flush();
				
				
				//message = (ByteFrame) fromOtherSide.readObject();
			} catch (ClassNotFoundException | InvalidKeyException | SignatureException e1) {
				System.exit(-1);
			} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
				if (useDebug)
	        	{
	        		System.out.println("There was some issue with reading a public key.");
	        	}
				continue;
			}
			
			//--------------------WAIT FOR BULK MESSAGE CONTAINING EVERYTHING FROM ALICE-----------------------
			
			if (useDebug)
        	{
        		System.out.println("Bob is waiting...");
        	}
			
			ByteFrame symObj = new ByteFrame();

			try {
				symObj = (ByteFrame) fromOtherSide.readObject();
				message = (ByteFrame) fromOtherSide.readObject();
			} catch (ClassNotFoundException e) {
				System.exit(-1);
			}
			
			byte[] keyBytes = null;
			try {
				keyBytes = SecureMessaging.DecryptArrayWithKey(
						Base64.getDecoder().decode(symObj.data), SecureMessaging.ReadPrivateKey("BobKeys/private_key.der"));
			} catch (InvalidKeyException | NoSuchAlgorithmException
					| NoSuchPaddingException | IllegalBlockSizeException
					| BadPaddingException | InvalidKeySpecException e) {
				if (useDebug)
	        	{
	        		System.out.println("There was some issue with decrypting a message.");
	        	}
				continue;
			}
			
			SecretKey secret = new SecretKeySpec(keyBytes, 0, keyBytes.length, "DESede");
			byte[] hashedMessage = null;
			try {
				hashedMessage = SecureMessaging.DecryptArrayWithSymKey(
						Base64.getDecoder().decode(message.data), secret);
			} catch (InvalidKeyException | NoSuchAlgorithmException
					| NoSuchPaddingException | IllegalBlockSizeException
					| BadPaddingException e) {
				if (useDebug)
	        	{
	        		System.out.println("There was some issue with decrypting a message.");
	        	}
				continue;
			}
			String messageText = new String(hashedMessage);
			
			String[] blocks = messageText.split("&&x&&");
			
			byte[] Hashed = Base64.getDecoder().decode(blocks[0]);
			
			byte[] decryptedMessage = null;
			
			try {
				decryptedMessage = SecureMessaging.DecryptArrayWithKey(Hashed, SecureMessaging.ReadPublicKey("BobKeys/alice_public_key.der"));
			} catch (InvalidKeyException | NoSuchAlgorithmException
					| NoSuchPaddingException | IllegalBlockSizeException
					| BadPaddingException | InvalidKeySpecException e) {
				if (useDebug)
	        	{
	        		System.out.println("There was some issue with decrypting a message.");
	        	}
				continue;
			}
			
			String normalStr = blocks[1];
			String hashedStr = new String(Base64.getDecoder().decode(decryptedMessage));
			
			try {
				normalStr = new String(SecureMessaging.HashMessage(normalStr.getBytes()));
			} catch (NoSuchAlgorithmException e) {
				return;
			}
				if (normalStr.equals(hashedStr))
				{
					//System.out.println("YES");
					if (useDebug)
		        	{
		        		System.out.println("Bob received message: " + blocks[1]);
		        	}
					
					messageArea.append("Alice: " + blocks[1] + "\n");
				}
        	
        	//System.out.println(DatatypeConverter.printHexBinary(pubKeyBytes));
        }

        socket.close();
    }

    /**
     * Runs the client as an application with a closeable frame.
     */
    public static void main(String[] args) throws Exception {
    	if (args.length > 0)
    	{
    		if (args[0].equals("-v"))
    		{
    			useDebug = true;
    		}
    	}
    	MessagingClient client = new MessagingClient();
        client.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        client.frame.setVisible(true);
        client.run();
    }


}
